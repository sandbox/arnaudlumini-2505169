core = 7.x
api = 2

libraries[phirehose][download][type] = git
libraries[phirehose][download][tag] = v1.0.1
libraries[phirehose][download][url] = https://github.com/fennb/phirehose.git
libraries[phirehose][destination] = "libraries"
libraries[phirehose][type] = "library"

libraries[system_daemon][download][type] = git
libraries[system_daemon][download][tag] = v1.0.0RC1-beta
libraries[system_daemon][download][url] = https://github.com/kvz/system_daemon.git
libraries[system_daemon][destination] = libraries
libraries[system_daemon][type] = library
