<?php

/**
 * Consume tweet data from the Steaming API and store in the database.
 */
class TwitterstreamPublicConsumer extends OauthPhirehose {

  /**
   * Create a new Streaming API consumer.
   */
  public function __construct($username, $password) {
    parent::__construct($username, $password, Phirehose::METHOD_FILTER);
  }

  /**
   * Set the minimum period between writing status updates to the log.
   *
   * @param int $value
   *   Number of seconds
   */
  public function setAvgPeriod($value = 60) {
    $this->avgPeriod = $value;
  }

  /**
   * Set the minimum period between checking for changes to the filter
   * predicates.
   *
   * The stream is only updated at most every 120 seconds, even if this period
   * is shorter.
   *
   * @param int $value
   *   Number of seconds
   */
  public function setFilterCheckMin($value = 5) {
    $this->filterCheckMin = $value;
  }

  /**
   * Put the provided raw status in the processing queue.
   *
   * @see Phirehose::enqueueStatus()
   */
  public function enqueueStatus($status) {
    DrupalQueue::get('twitterstream_process')
      ->createItem($status);
  }

  /**
   * Fetch the keywords to track and users to follow.
   *
   * @see Phirehose::checkFilterPredicates()
   */
  public function checkFilterPredicates() {

    // TODO Connecting to the Streaming API will fail if neither parameter is
    // specified (causing the daemon to exit after several retries), so sleep
    // and check again if nothing is available?

    $track = array();
    $follow = array();

    $result = Database::getConnection()
      ->query("SELECT params FROM {twitterstream_params}");
    foreach ($result as $row) {
      $params = unserialize($row->params);
      if (!empty($params['track'])) {
        $track = array_merge($track, $params['track']);
      }
      if (!empty($params['follow'])) {
        $follow = array_merge($follow, $params['follow']);
      }
    }

    $this->setTrack($track);
    $this->setFollow($follow);
  }

  /**
   * Connects to the stream URL using the configured method.
   *
   * Phirehose::connect() only attempts to resolve the IP address of the
   * Streaming API endpoint once, so this method catches the exception and
   * attempts to connect again after a delay.
   *
   * @throws ErrorException
   *
   * @see Phirehose::connect()
   */
  protected function connect() {
    $networkFailures = 0;

    while (TRUE) {
      try {
        parent::connect();
        return;
      } catch (PhirehoseNetworkException $ex) {

        $networkFailures++;

        if ($networkFailures >= 5) {
          throw $ex;
        }

        $this->log($ex->getMessage());

        sleep(pow(2, $networkFailures));
      }
    }
  }

  /**
   * Pass log messages through to System_Daemon::log().
   *
   * @param string $message
   * @param string $level
   */
  public function log($message, $level = 'notice') {
    System_Daemon::log(System_Daemon::LOG_INFO, 'Phirehose: ' . $message);
  }
}
